// C:\Users\sdkca\Desktop\electron-workspace\build.js
var electronInstaller = require('electron-winstaller');

// In this case, we can use relative paths
var settings = {
    // Specify the folder where the built app is located
    appDirectory: './HypeSquadRPC-win32-x64',
    // Specify the existing folder where
    outputDirectory: './HypeSquadRPC-built-installers',
    // The name of the Author of the app (the name of your company)
    authors: 'Jengas',
    // The name of the executable of your built
    exe: './HypeSquadRPC.exe',
	loadingGif: "./assets/img/loading.gif",
	setupIcon: "./assets/img/icon.ico",
  iconUrl: "https://i.imgur.com/v0d4Q9j.png"
};

resultPromise = electronInstaller.createWindowsInstaller(settings);

resultPromise.then(() => {
    console.log("The installers of your application were succesfully created !");
}, (e) => {
    console.log(`Well, sometimes you are not so lucky: ${e.message}`)
});
